/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/draw.js":
/*!*********************!*\
  !*** ./src/draw.js ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"drawPoints\": () => (/* binding */ drawPoints),\n/* harmony export */   \"drawLineConnect\": () => (/* binding */ drawLineConnect),\n/* harmony export */   \"drawLineNormal\": () => (/* binding */ drawLineNormal)\n/* harmony export */ });\n/* harmony import */ var _test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./test */ \"./src/test.js\");\n\nconst drawPoints = ({\n  x,\n  y,\n  r,\n  color\n}, ctx) => {\n  ctx.beginPath();\n  ctx.fillStyle = color;\n  ctx.arc(x, y, r, 0, Math.PI * 2, true);\n  ctx.fill();\n};\nconst drawLineConnect = (ctx, points, point) => {\n  ctx.strokeStyle = \"#000\";\n  ctx.beginPath();\n  ctx.moveTo(point.x, point.y);\n  points.forEach(pt => {\n    const {\n      x,\n      y,\n      id\n    } = pt;\n\n    if (point.id !== id) {\n      ctx.lineTo(x, y);\n    }\n  });\n  ctx.stroke();\n};\nconst drawLineNormal = (ctx, points, point) => {\n  const {\n    x: x0,\n    y: y0,\n    r: r0,\n    color,\n    vx: vx0,\n    vy: vy0\n  } = point;\n  ctx.beginPath();\n  points.map(pt => {\n    if (point.id !== pt.id) {\n      const {\n        x: x1,\n        y: y1,\n        color,\n        r: r1,\n        vx,\n        vy\n      } = pt;\n      const distance = (0,_test__WEBPACK_IMPORTED_MODULE_0__.getDistance)(x0, x1, y0, y1);\n      const cosA = (0,_test__WEBPACK_IMPORTED_MODULE_0__.getAlpha)(x0, x1, distance);\n      const xt0 = r0 / cosA + x0;\n      const yt0 = y0;\n      const xt1 = r0 * cosA + x0;\n      const yt1 = r0 * Math.sqrt(1 - Math.pow(cosA, 2)) * (y1 - y0 > 0 ? 1 : -1) + y0;\n      ctx.fillStyle = color;\n      ctx.beginPath();\n      ctx.arc(xt1, yt1, r0 / 20, 0, Math.PI * 2, true);\n      ctx.fill();\n      ctx.strokeStyle = \"#fff\";\n\n      if (distance < r0 + r1) {\n        ctx.strokeStyle = \"red\";\n      }\n\n      ctx.moveTo(x0, y0);\n      ctx.lineTo(xt0, yt0);\n      ctx.lineTo(xt1, yt1);\n      ctx.stroke();\n      ctx.beginPath();\n      ctx.moveTo(x0, y0);\n      ctx.lineTo(xt1, yt1);\n      ctx.stroke();\n    }\n  });\n  ctx.fillStyle = \"#000\";\n  ctx.beginPath();\n  ctx.arc(x0, y0, Math.sqrt(Math.pow(vy0, 2) + Math.pow(vx0, 2)) * 50, 0, Math.PI * 2, true);\n  ctx.fill();\n  ctx.strokeStyle = \"#fff\";\n  ctx.beginPath();\n  ctx.moveTo(x0, y0);\n  ctx.lineTo(vx0 * r0 + x0, vy0 * r0 + y0);\n  ctx.stroke();\n};\n\n//# sourceURL=webpack://chaotic/./src/draw.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./test */ \"./src/test.js\");\n/* harmony import */ var _draw__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./draw */ \"./src/draw.js\");\n/* harmony import */ var _canvas_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./canvas.scss */ \"./src/canvas.scss\");\n/* harmony import */ var _move__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./move */ \"./src/move.js\");\n\n\n\n\nconst canvas = document.getElementById(\"canvas\");\nconst btnSTART = document.getElementById(\"start\");\nconst btnSTOP = document.getElementById(\"stop\");\n\nconst setCanWH = () => {\n  canvas.setAttribute(\"width\", window.innerWidth);\n  canvas.setAttribute(\"height\", window.innerHeight);\n  const {\n    clientWidth: wC,\n    clientHeight: hC\n  } = canvas;\n  return {\n    wC,\n    hC\n  };\n};\n\nsetCanWH();\nconst speedBall = 2;\nconst radiusBall = 50;\nconst countBall = 10;\n\nconst animWrap = () => {\n  const anim = pts => {\n    const canvas = document.getElementById(\"canvas\");\n    const ctx = canvas.getContext(\"2d\");\n    ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);\n    const newPts = pts.map(p => {\n      (0,_draw__WEBPACK_IMPORTED_MODULE_1__.drawPoints)(p, ctx); // drawLineConnect(ctx, pts, p)\n      // drawLineNormal(ctx, pts, p);\n      // testCollision(tempPts0[i], tempPts0);\n      //console.log(tempPts0);\n\n      const tempPt = (0,_move__WEBPACK_IMPORTED_MODULE_3__.move)(p, pts); //console.log(tempPt);\n\n      /* const arr = pts.map(({id})=>{\r\n        return (id === tempPt) ? \r\n      }) */\n      // return newSpeedPoints(pts, tempPt);\n\n      return tempPt;\n    });\n    const requestId = window.requestAnimationFrame(() => anim(newPts));\n    btnSTOP.addEventListener(\"click\", () => window.cancelAnimationFrame(requestId));\n  };\n\n  const pointss = (0,_test__WEBPACK_IMPORTED_MODULE_0__.cyrcleGen)(setCanWH().wC, setCanWH().hC, radiusBall, countBall, speedBall);\n  anim(pointss);\n};\n\nbtnSTART.addEventListener(\"click\", () => {\n  animWrap();\n});\n\n//# sourceURL=webpack://chaotic/./src/index.js?");

/***/ }),

/***/ "./src/move.js":
/*!*********************!*\
  !*** ./src/move.js ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"move\": () => (/* binding */ move)\n/* harmony export */ });\n/* harmony import */ var _test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./test */ \"./src/test.js\");\n\nconst move = (point, pts) => {\n  const {\n    x,\n    y,\n    v\n  } = point;\n  const pt = (0,_test__WEBPACK_IMPORTED_MODULE_0__.testBorder)(point);\n  const {\n    vxs,\n    vys,\n    resPoint\n  } = (0,_test__WEBPACK_IMPORTED_MODULE_0__.newSpeedPoints)(pts, pt);\n  return { ...resPoint,\n    x: x + vxs * v,\n    y: y + vys * v,\n    vx: vxs,\n    vy: vys\n  };\n};\n\n//# sourceURL=webpack://chaotic/./src/move.js?");

/***/ }),

/***/ "./src/test.js":
/*!*********************!*\
  !*** ./src/test.js ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"cyrcleGen\": () => (/* binding */ cyrcleGen),\n/* harmony export */   \"testBorder\": () => (/* binding */ testBorder),\n/* harmony export */   \"testCollision\": () => (/* binding */ testCollision),\n/* harmony export */   \"createLineNormal\": () => (/* binding */ createLineNormal),\n/* harmony export */   \"getDistance\": () => (/* binding */ getDistance),\n/* harmony export */   \"getAlpha\": () => (/* binding */ getAlpha),\n/* harmony export */   \"newSpeedPoints\": () => (/* binding */ newSpeedPoints)\n/* harmony export */ });\nconst cyrcleGen = (wC = 250, hC = 250, radiusBall = 10, countBall = 10, speedBall = 0) => {\n  return [...new Array(countBall)].map(() => {\n    const vx = Math.random() * 2 - 1;\n    const vy = Math.round(Math.random()) ? 1 - vx : -(1 - vx);\n    return {\n      id: Math.round(Math.random() * 1000),\n      x: Math.round(Math.random() * (wC - 2 * radiusBall) + radiusBall),\n      y: Math.round(Math.random() * (hC - 2 * radiusBall) + radiusBall),\n      v: Math.round(Math.random() * speedBall),\n      vx,\n      vy,\n      r: radiusBall,\n      color: `rgb(${Math.random() * 150 + 50}, ${Math.random() * 150 + 50}, ${Math.random() * 150 + 50})`\n    };\n  });\n};\nconst testBorder = point => {\n  const {\n    x,\n    y,\n    r,\n    vx,\n    vy\n  } = point;\n  const w = window.innerWidth;\n  const h = window.innerHeight;\n\n  if (x - r <= 0) {\n    return { ...point,\n      vx: Math.abs(vx)\n    };\n  }\n\n  if (x + r >= w) {\n    return { ...point,\n      vx: -Math.abs(vx)\n    };\n  }\n\n  if (y - r <= 0) {\n    return { ...point,\n      vy: Math.abs(vy)\n    };\n  }\n\n  if (y + r >= h) {\n    return { ...point,\n      vy: -Math.abs(vy)\n    };\n  }\n\n  return point;\n};\nconst testCollision = (point, points) => {};\nconst createLineNormal = () => {};\nconst getDistance = (x0, x1, y0, y1) => {\n  const distance = Math.sqrt(Math.pow(x1 - x0, 2) + Math.pow(y1 - y0, 2));\n  return distance;\n};\nconst getAlpha = (x0, x1, distance) => {\n  const cosA = (x1 - x0) / distance;\n  return cosA;\n};\nconst newSpeedPoints = (points, point) => {\n  const {\n    x: x0,\n    y: y0,\n    id: id0,\n    r: r0,\n    vx: vx0,\n    vy: vy0\n  } = point;\n  let vxs = vx0;\n  let vys = vy0;\n  points.forEach(pt => {\n    if (pt.id !== id0) {\n      const {\n        x: x1,\n        y: y1,\n        r: r1\n      } = pt;\n      const distance = getDistance(x0, x1, y0, y1);\n\n      if (distance <= r0 + r1) {\n        const cosA = getAlpha(x0, x1, distance);\n        const gipo = Math.sqrt(Math.pow(vy0, 2) + Math.pow(vx0, 2));\n        const angleA = Math.acos(cosA) * 180 / Math.PI;\n        const angleB = Math.atan(vy0 / vx0) * 180 / Math.PI;\n        const angleAB = angleA + angleB;\n        vxs = gipo * Math.sin(Math.PI / 180 * angleAB);\n        vys = gipo * Math.cos(Math.PI / 180 * angleAB);\n        const D = (vxs - 0) * (vy0 - 0) - (vys - 0) * (vx0 - 0);\n        vxs = D < 0 && x1 - x0 < 0 ? vxs : -1 * Math.abs(vxs);\n        vys = D < 0 && y1 - y0 < 0 ? vys : -1 * Math.abs(vys);\n      }\n    }\n  });\n  return {\n    vxs,\n    vys,\n    resPoint: point\n  };\n};\n/* export const newSpeedPoints = (points, point) => {\r\n  const { x: x0, y: y0, id: id0, r: r0, vx, vy } = point;\r\n\r\n  const { vx: vx1, vy: vy1 } = points.reduce(\r\n    (acc, pt) => {\r\n      const { x: x1, y: y1, id, vx, vy, r: r1 } = pt;\r\n      if (id0 !== id) {\r\n        const distance = getDistance(x0, x1, y0, y1);\r\n        const cosA = getAlpha(x0, x1, distance);\r\n        if (distance < r0 + r1) {\r\n          const gipo = Math.sqrt(Math.pow(vy, 2) + Math.pow(vx, 2));\r\n          const angleA = (Math.acos(cosA) * 180) / Math.PI;\r\n          const angleB = (Math.atan(vy / vx) * 180) / Math.PI;\r\n          const angleAB = angleA + angleB;\r\n          console.log(gipo, vy, vx);\r\n\r\n          const nVy = -1 * gipo * Math.sin((Math.PI / 180) * angleAB);\r\n          const nVx = gipo * Math.cos((Math.PI / 180) * angleAB);\r\n          console.log(nVx, nVy);\r\n          return {\r\n            vx1: nVx,\r\n            vy1: nVy,\r\n          };\r\n        }\r\n        return acc;\r\n      }\r\n      return acc;\r\n    },\r\n    { vx, vy }\r\n  );\r\n  return {\r\n    ...point,\r\n    vx: vx1,\r\n    vy: vy1,\r\n  };\r\n}; */\n\n/* \r\nconst line = {\r\n  x1: 0,\r\n  y1: setCanWH().hC / 2,\r\n  x2: setCanWH().wC,\r\n  y2: setCanWH().hC / 1.2,\r\n};\r\n\r\nconst cyrcleGen = () => {\r\n  const { wC, hC } = setCanWH();\r\n  const vx = Math.random() * 2 - 1;\r\n  const vy = (1 - Math.abs(vx)) * (Math.round(Math.random()) * 2 - 1) || 1;\r\n  return [...new Array(countP)].map(() => {\r\n    return {\r\n      id: Math.round(Math.random() * 1000),\r\n      x: Math.round(Math.random() * (wC - 2 * rPoint) + rPoint),\r\n      y: Math.round(Math.random() * (hC - 2 * rPoint) + rPoint),\r\n      v: Math.round(Math.random() * speed) + 10,\r\n      vx,\r\n      vy,\r\n      r: rPoint,\r\n      color: `rgb(${Math.random() * 150 + 50}, ${Math.random() * 150 + 50}, ${\r\n        Math.random() * 150 + 50\r\n      })`,\r\n    };\r\n  });\r\n}; */\n// const D = (х3 - х1) * (у2 - у1) - (у3 - у1) * (х2 - х1)\n// - Если D = 0 - значит, точка С лежит на прямой АБ.\n// - Если D < 0 - значит, точка С лежит слева от прямой.\n// - Если D > 0 - значит, точка С лежит справа от прямой.\n\n/* \r\nconst fnCross = (points) => {\r\n\r\n  return points.map((pt, i) => {\r\n    if (id !== pt.id) {\r\n      const cosAlpha = (x - pt.x) / (y - pt.y);\r\n      const katTX = r * cosAlpha;\r\n      const katTY = katTX / cosAlpha;\r\n\r\n      const x1 = x + katTX;\r\n      const y1 = y + katTY;\r\n\r\n      const kat1 = r / cosAlpha;\r\n      const x2 = x + Math.sqrt(Math.pow(kat1, 2) + Math.pow(r, 2));\r\n      const y2 = y;\r\n      //       if (i === 1) {\r\n      //   console.log(x1, y1);\r\n      // }\r\n      return {\r\n        id: Math.round(Math.random() * 1000),\r\n        x: x1,\r\n        y: y1,\r\n        r: 8,\r\n        color: \"#fff\",\r\n      };\r\n    }\r\n  });\r\n}; */\n\n/* \r\nconst test = (point, points) => {\r\n  const { wC: w, hC: h } = setCanWH();\r\n  const { x, y, vx, vy, r, id } = point;\r\n  let sx = vx;\r\n  let sy = vy;\r\n\r\n  const { x1, y1, x2, y2 } = line;\r\n\r\n  const D = (x - x1) * (y2 - y1) - (y - y1) * (x2 - x1) < 0;\r\n\r\n  const distToLine = Math.round(\r\n    Math.abs(\r\n      Math.abs((y2 - y1) * x - (x2 - x1) * y + x2 * y1 - y2 * x1) /\r\n        Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2))\r\n    )\r\n  );\r\n\r\n  if (D && distToLine < r) {\r\n    sy = Math.abs(vy);\r\n  }\r\n\r\n  if (x - r <= 0) {\r\n    sx = -vx;\r\n  }\r\n  if (x + r >= w) {\r\n    sx = -vx;\r\n  }\r\n\r\n  if (y - r <= 0) {\r\n    sy = -vy;\r\n  }\r\n  if (y + r >= h) {\r\n    sy = -vy;\r\n  }\r\n\r\n  return { vx: sx, vy: sy };\r\n};\r\n */\n\n/* const move = (points = []) => {\r\n  const newPoints = fnCross();\r\n  return points\r\n    .map((point) => {\r\n      const { vx, vy } = test(point, points);\r\n      return {\r\n        ...points,\r\n        //x: point.x + vx * point.v,\r\n        // y: point.y + vy * point.v,\r\n        //vx,\r\n        //vy,\r\n      };\r\n    })\r\n    .concat(newPoints);\r\n};\r\n\r\nconst drawLine = (ctx, line) => {\r\n  const { x1, y1, x2, y2 } = line;\r\n  ctx.strokeStyle = \"rgb(255, 150, 150)\";\r\n  ctx.beginPath();\r\n  ctx.moveTo(x1, y1);\r\n  ctx.lineTo(x2, y2);\r\n  ctx.stroke();\r\n};\r\n\r\nconst drawBacground = (ctx, wC, hC) => {\r\n  ctx.fillStyle = \"rgb(50, 50, 50)\";\r\n  ctx.fillRect(0, 0, wC, hC);\r\n};\r\n\r\nconst drawCyrcle = (ctx, points) => {\r\n  points.forEach(({ x, y, r, color }) => {\r\n    ctx.beginPath();\r\n    ctx.fillStyle = color;\r\n    //ctx.strokeStyle = \"rgb(255, 150, 150)\";\r\n    ctx.arc(x, y, r, 0, Math.PI * 2, true);\r\n    ctx.fill();\r\n    //ctx.stroke();\r\n  });\r\n};\r\n\r\nconst draw = (wC, hC, points) => {\r\n  const ctx = canvas.getContext(\"2d\");\r\n  drawBacground(ctx, wC, hC);\r\n  drawCyrcle(ctx, points);\r\n  drawLine(ctx, line);\r\n};\r\n\r\nconst anim = (pts) => {\r\n  let points = pts;\r\n  requestAnimationFrame(function () {\r\n    const { wC, hC } = setCanWH();\r\n    points = move(points);\r\n    draw(wC, hC, points);\r\n    requestAnimationFrame(() => anim(points));\r\n  });\r\n};\r\n\r\nconst start = (pts) => {\r\n  anim(pts);\r\n}; */\n\n/* const cosAlpha = x - pt.x / y - pt.y;\r\n    const katTX = r * cosAlpha;\r\n    const katTY = katTX / cosAlpha;\r\n\r\n    const x1 = x + katTX;\r\n    const y1 = y + katTY;\r\n\r\n    const kat1 = r / cosAlpha;\r\n    const x2 = x + Math.sqrt(Math.pow(kat1, 2) + Math.pow(r, 2));\r\n    const y2 = y;\r\n\r\n    const D = (x - x1) * (y2 - y1) - (y - y1) * (x2 - y1); */\n\n/*   points.forEach((pt) => {\r\n    const katX = Math.abs(x - pt.x);\r\n    const katY = Math.abs(y - pt.y);\r\n    const coof = katY / katX;\r\n    const distance = Math.sqrt(Math.pow(katX, 2) + Math.pow(katY, 2));\r\n    const distCollis = r + pt.r;\r\n    if (distance < distCollis && id !== pt.id) {\r\n      if (coof > 0) {\r\n        sx = Math.abs(vx) * coof;\r\n        sy = Math.abs(vy) * (1 / coof);\r\n      }\r\n      sx = -vx;\r\n      sy = -vy;\r\n    }\r\n  }); */\n\n//# sourceURL=webpack://chaotic/./src/test.js?");

/***/ }),

/***/ "./src/canvas.scss":
/*!*************************!*\
  !*** ./src/canvas.scss ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://chaotic/./src/canvas.scss?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/ 	
/******/ })()
;