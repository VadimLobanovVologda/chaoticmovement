import { initCanvas } from "./initCanvas";
import { getFps, setBorderCanvas } from "./helpers/helpers";
import { main } from "./main";
import "./canvas.scss";
import { ballsGenerate } from "./main/balls";

const { canvas, ctx, btnSTART, btnSTOP, fpxDisplay } = initCanvas();

const animation = (balls, tick, date) => {
  setBorderCanvas(canvas);
  const now = Date.now();
  const fps = getFps(now, date);
  if (tick % 5 === 0) fpxDisplay.value = fps;

  const ballsNow = main(canvas, ctx, balls);

  window.RAF = window.requestAnimationFrame(() =>
    animation(ballsNow, tick + 1, now)
  );
};

const rBalls = 50;
const cBalls = 200;

btnSTART.addEventListener("click", () => {
  const balls = ballsGenerate(cBalls, rBalls, canvas);
  window.cancelAnimationFrame(window.RAF);
  const date = Date.now();
  animation(balls, 0, date);
});
btnSTOP.addEventListener("click", () =>
  window.cancelAnimationFrame(window.RAF)
);
