import { getAlpha, getDistance } from "./test";

export const drawPoints = ({ x, y, r, color }, ctx) => {
  ctx.beginPath();
  ctx.fillStyle = color;
  ctx.arc(x, y, r, 0, Math.PI * 2, true);
  ctx.stroke();
  // ctx.fill();
};

export const drawLineConnect = (ctx, points, point) => {
  ctx.strokeStyle = "#000";
  ctx.beginPath();
  ctx.moveTo(point.x, point.y);
  points.forEach((pt) => {
    const { x, y, id } = pt;
    if (point.id !== id) {
      ctx.lineTo(x, y);
    }
  });
  ctx.stroke();
};

export const drawLineNormal = (ctx, points, point) => {
  const { x: x0, y: y0, r: r0, vx: vx0, vy: vy0 } = point;

  ctx.beginPath();
  points.map((pt) => {
    if (point.id !== pt.id) {
      const { x: x1, y: y1, color, r: r1, vx, vy } = pt;
      const distance = getDistance(x0, x1, y0, y1);
      const cosA = getAlpha(x0, x1, distance);

      const xt0 = r0 / cosA + x0;
      const yt0 = y0;

      const xt1 = r0 * cosA + x0;
      const yt1 =
        r0 * Math.sqrt(1 - Math.pow(cosA, 2)) * (y1 - y0 > 0 ? 1 : -1) + y0;

/*       ctx.fillStyle = color;
      ctx.beginPath();
      ctx.arc(xt1, yt1, r0 / 20, 0, Math.PI * 2, true);
      ctx.fill(); */

      ctx.strokeStyle = "#fff";
      if (distance < r0 + r1) {
        ctx.strokeStyle = "red";
      }
      ctx.moveTo(x0, y0);
      ctx.lineTo(xt0, yt0);
      ctx.lineTo(xt1, yt1);
      ctx.stroke();
      


    }
  });

  ctx.fillStyle = "#000";
  ctx.beginPath();
  ctx.arc(
    x0,
    y0,
    Math.sqrt(Math.pow(vy0, 2) + Math.pow(vx0, 2)) * 7,
    0,
    Math.PI * 2,
    true
  );
  ctx.fill();

/*   ctx.strokeStyle = "#000";
  ctx.beginPath();
  ctx.moveTo(x0, y0);
  ctx.lineTo(vx0 * r0 + x0, vy0 * r0 + y0);
  ctx.stroke(); */
};
