import { getDistance, newSpeedPoints, testBorder } from "./test";

export const move = (point) => {
  //const { vxs, vys, resPoint } = newSpeedPoints(pts, point);
  const pt0 = testBorder(point);
  const { x, y, v } = pt0;

  return {
    ...pt0,
    x: x + pt0.vx * v,
    y: y + pt0.vy * v,
  };
};
