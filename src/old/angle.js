import { getAngle360, getSide } from "./helpers";

export const drawAngleObstacle = (ctx, x1, y1, { x: x0, y: y0 }) => {
  ctx.strokeStyle = "#fff";
  ctx.beginPath();
  ctx.moveTo(x1 + x0, y1 + y0);
  ctx.lineTo(x0, y0);
  ctx.lineTo(x0 - x1, y0 - y1);
  ctx.stroke();
};

export const drawAngleSpeed = (ctx, angle, { x: xc, y: yc }) => {
  const { x0, y0, xf0, yf0, x1, y1, xf1, yf1 } = angle;

  ctx.lineWidth = 3;
  ctx.strokeStyle = "#f00";
  ctx.beginPath();
  ctx.moveTo(x0 + xc, y0 + yc);
  ctx.lineTo(xc, yc);
  ctx.stroke();

  ctx.strokeStyle = "#0ff";
  ctx.beginPath();
  ctx.moveTo(xf0 + xc, yf0 + yc);
  ctx.lineTo(xc, yc);
  ctx.stroke();

  ctx.lineWidth = 3;
  ctx.strokeStyle = "#f00";
  ctx.beginPath();
  ctx.moveTo(x1 + xc, y1 + yc);
  ctx.lineTo(xc, yc);
  ctx.stroke();

  ctx.strokeStyle = "#0ff";
  ctx.beginPath();
  ctx.moveTo(xf1 + xc, yf1 + yc);
  ctx.lineTo(xc, yc);
  ctx.stroke();
};

export const setAngleSpeed = (
  ctx,
  ball0,
  ball1,
  angle0,
  angle1,
  angleObstacle,
  center
) => {
  const angleRadian0 = (getAngle360(angle0) * Math.PI) / 180;
  const angleRadian1 = (getAngle360(angle1) * Math.PI) / 180;
  const obstRadianM90 = ((90 - getAngle360(angleObstacle)) * Math.PI) / 180;
  const obstRadian = (getAngle360(angleObstacle) * Math.PI) / 180;

  const V = 350; // условно
  const xObst = V * Math.cos(obstRadian);
  const yObst = V * Math.sin(obstRadian);
  const { x: xb0, y: yb0 } = ball0;
  const { x: xb1, y: yb1 } = ball1;
  drawBallls(ctx, xb0, yb0, xb1, yb1, center);
  const dBall0 = getSide(0, 0, xObst, -yObst, xb0, yb0);
  const dBall1 = getSide(0, 0, xObst, -yObst, xb1, yb1);



  const x0 = V * Math.cos(angleRadian0);
  const y0 = V * Math.sin(angleRadian0); // угол падения
  const xn0 = x0 * Math.cos(obstRadianM90) - y0 * Math.sin(obstRadianM90);
  const yn0 = x0 * Math.sin(obstRadianM90) + y0 * Math.cos(obstRadianM90); // угол в новых коорд
  const xf0 = -xn0 * Math.cos(obstRadianM90) + yn0 * Math.sin(obstRadianM90);
  const yf0 =
    -1 * -xn0 * Math.sin(obstRadianM90) + yn0 * Math.cos(obstRadianM90);

  const x1 = V * Math.cos(angleRadian1);
  const y1 = V * Math.sin(angleRadian1); // угол падения
  const xn1 = x1 * Math.cos(obstRadianM90) - y1 * Math.sin(obstRadianM90);
  const yn1 = x1 * Math.sin(obstRadianM90) + y1 * Math.cos(obstRadianM90); // угол в новых коорд
  const xf1 = -xn1 * Math.cos(obstRadianM90) + yn1 * Math.sin(obstRadianM90);
  const yf1 =
    -1 * -xn1 * Math.sin(obstRadianM90) + yn1 * Math.cos(obstRadianM90);

  const res = {
    x0,
    y0,
    xf0,
    yf0,
    x1,
    y1,
    xf1,
    yf1,
  };

  drawAngleSpeed(ctx, res, center);


  const d0Vector = getSide(0, 0, xObst, yObst, x0, y0);
  const d1Vector = getSide(0, 0, xObst, yObst, x1, y1);



  return {
    d0: d0Vector >= 0,
    d1: d1Vector >= 0,
    d2: dBall0 >= 0,
    d3: dBall1 >= 0,
    rebound: (d0Vector >= 0 && d1Vector >= 0) === !(dBall0 < 0),
  };
};

const drawBallls = (ctx, xb0, yb0, xb1, yb1, center) => {
  ctx.beginPath();
  ctx.fillStyle = "red";
  ctx.arc(xb0, yb0, 10, 0, Math.PI * 2, true);
  ctx.fill();
  ctx.beginPath();
  ctx.fillStyle = "green";
  ctx.arc(xb1, yb1, 10, 0, Math.PI * 2, true);
  ctx.fill();
};
