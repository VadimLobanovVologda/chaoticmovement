import {
  cyrcleGen,
  getAlpha,
  getDistance,
  newSpeedPoints,
  testCollision,
} from "./test";
import { drawLineConnect, drawLineNormal, drawPoints } from "./draw";
import "./canvas.scss";
import { move } from "./move";
import { drawAngleObstacle, drawAngleSpeed, setAngleSpeed } from "./angle";
import { getAngle360 } from "./helpers";

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

const btnSTART = document.getElementById("start");
const btnSTOP = document.getElementById("stop");
const angleVector0 = document.getElementById("angleVector0");
const angleVector1 = document.getElementById("angleVector1");
const obstacle = document.getElementById("obstacle");
obstacle.value = 0;
angleVector0.value = 0;
angleVector1.value = 0;
const resultInput = document.getElementById("resultInput");
const setCanWH = () => {
  canvas.setAttribute("width", document.body.clientWidth);
  canvas.setAttribute("height", document.body.clientHeight);
  const { clientWidth: wC, clientHeight: hC } = canvas;
  return { wC, hC };
};
setCanWH();
let center = {
  x: setCanWH().wC / 2,
  y: setCanWH().hC / 2,
};
const getCenter = () => {
  center = {
    x: setCanWH().wC / 2,
    y: setCanWH().hC / 2,
  };
};
// ///// initial ////////
// //////////////////////

const setAngleObstacle = (angle, { xt1: x, yt1: y }) => {
  const angleRadian = ((angle + 90) * Math.PI) / 180;
  const V = 350;
  const XX = V * Math.cos(angleRadian);
  const YY = V * Math.sin(angleRadian);

  // drawAngleObstacle(ctx, XX, YY, { x, y });
};

const speedBall = 2;
const radiusBall = 120;
const countBall = 2;

const displayAngle = (pt0, pt1, angleObstacle, xt1, yt1) => {
  const obstValue = angleObstacle + obstacle.value;
  const angleSpeed0 = 0; // angleVector0.value;
  const angleSpeed1 = 0; // angleVector1.value;
  setAngleObstacle(angleObstacle, { xt1, yt1 });

  const ball0 = {
    x: pt0.x,
    y: pt0.y,
  };
  const ball1 = {
    x: pt1.x,
    y: pt1.y,
  };

  // const { d0, d1, d2, d3, rebound } = setAngleSpeed(
  //   ctx,
  //   ball0,
  //   ball1,
  //   angleSpeed0,
  //   angleSpeed1,
  //   obstValue,
  //   center
  // );

  resultInput.innerHTML = `
  <input type="text" value="D0:${angleObstacle}, RES: ${"rebound" || ""}"/
  >`;
};

const animWrap = () => {
  getCenter();
  const anim = (pts) => {
    ctx.clearRect(0, 0, document.body.clientWidth, document.body.clientHeight);

    pts.forEach((pt0) => {
      const { x: x0, y: y0, r: r0 } = pt0;
      pts.forEach((pt1) => {
        const { x: x1, y: y1 } = pt1;
        const distance = getDistance(x0, x1, y0, y1);
        const cosA = getAlpha(x0, x1, distance);
        const alphaObs = (Math.acos(cosA) * 180) / Math.PI;
        const xt0 = r0 / cosA + x0;
        const yt0 = y0;
        const xt1 = r0 * cosA + x0;
        const yt1 =
          r0 * Math.sqrt(1 - Math.pow(cosA, 2)) * (y1 - y0 > 0 ? 1 : -1) + y0;
          
        if (pt0.id !== pt1.id) {
          ctx.fillStyle = "gold";
          ctx.beginPath();
          ctx.arc(xt0, yt0, r0 / 20, 0, Math.PI * 2, true);
          ctx.fill();

          ctx.fillStyle = "gold";
          ctx.beginPath();
          ctx.arc(xt1, yt1, r0 / 20, 0, Math.PI * 2, true);
          ctx.fill();

          displayAngle(pt0, pt1, alphaObs, xt1, yt1);
          drawLineNormal(ctx, pts, pt0);
        }
      });
      drawPoints(pt0, ctx);
    });

    const requestId = window.requestAnimationFrame(() => anim(pts)); // newPts
    btnSTOP.addEventListener("click", () =>
      window.cancelAnimationFrame(requestId)
    );
  };

  const pointss = cyrcleGen(
    setCanWH().wC,
    setCanWH().hC,
    radiusBall,
    countBall,
    speedBall
  );
  anim(pointss);
};
btnSTART.addEventListener("click", () => {
  animWrap();
});

/*
    const { x: x1, y: y1, r: r1, vx: vx1, vy: vy1 } = pt;
    
    const cosA = getAlpha(x0, x1, distance);
    const A = Math.acos(cosA);

    const vx0T = vx1 * Math.cos(A) + vy1 * Math.sin(A);
    const vy0T = -1 * vx1 * Math.sin(A) + vy1 * Math.cos(A);
    const vx1T = vx0T;
    const vy1T = -1 * vy0T;
    nvx = vx1T * Math.cos(A) - vy1T * Math.sin(A);
    nvy = vx1T * Math.sin(A) + vy1T * Math.cos(A);
    if (distance < r0 + r1) {
      nvx = vy1;
      nvy = vx1;
    }
    */

/* const convertVxy = (point, points, ctx) => {
  const { x: x0, y: y0, r: r0, vx: vx0, vy: vy0, id: id0, v: v0 } = point;

  let VX0r = vx0;
  let VY0r = vy0;

  points.forEach((pt) => {
    const { x: x1, y: y1, r: r1, vx: vx1, vy: vy1, id: id1 } = pt;
    if (id0 !== id1) {
      //
      const distance = Math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
      const cosA = (x1 - x0) / distance;
      const A0 = (Math.acos(cosA) * 180) / Math.PI;

      const vx0T = vx1 * Math.cos(A0) + vy1 * Math.sin(A0);
      const vy0T = -1 * vx1 * Math.sin(A0) + vy1 * Math.cos(A0);
      const vx1T = vx0T;
      const vy1T = -vy0T;
      const nvx = vx1T * Math.cos(A0) - vy1T * Math.sin(A0);
      const nvy = vx1T * Math.sin(A0) + vy1T * Math.cos(A0);
      //       if (distance < r0 + r1) {
      //   VX0r = nvx;
      //   VY0r = nvy;
      // }
      const c = 30;

      ctx.fillStyle = "red";
      ctx.beginPath();
      ctx.arc(x0, y0, r0 / 20, 0, Math.PI * 2, true);
      ctx.fill();

      ctx.strokeStyle = "red";
      ctx.beginPath();
      ctx.moveTo(x0, y0);
      ctx.lineTo(nvx * c + x0, nvy * c + y0);
      ctx.stroke();

      resultInput.innerHTML = `<input type="text" value="${Math.round(
        distance - r1 - r0
      )}, ${Math.round(A0)}, ${Math.round(-A0)}"  />`;
    }
  });

  return {
    ...point,
    vx: VX0r,
    vy: VY0r,
  };
}; */

// const newPts = pts.map((p) => {
//   drawPoints(p, ctx);
//   //drawLineNormal(ctx, pts, p);
//   //const newPt = convertVxy(p, pts, ctx);

//   const tempPt = move(p, pts);
//   return tempPt;
// });
