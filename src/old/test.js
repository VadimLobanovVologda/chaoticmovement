export const cyrcleGen = (
  wC = 250,
  hC = 250,
  radiusBall = 10,
  countBall = 10,
  speedBall = 0
) => {
  return [...new Array(countBall)].map(() => {
    const vx = Math.random() * 2 - 1;
    const vy = Math.round(Math.random()) ? 1 - vx : -(1 - vx);
    return {
      id: Math.round(Math.random() * 1000),
      x: Math.round(Math.random() * (wC - 2 * radiusBall) + radiusBall),
      y: Math.round(Math.random() * (hC - 2 * radiusBall) + radiusBall),
      v: Math.random() * speedBall,
      vx,
      vy,
      r: radiusBall,
      color: `rgb(${Math.random() * 150 + 50}, ${Math.random() * 150 + 50}, ${
        Math.random() * 150 + 50
      })`,
    };
  });
};

export const testBorder = (point) => {
  const { x, y, r, vx, vy } = point;
  const w = document.body.clientWidth;
  const h = document.body.clientHeight;

  if (x - r < 0) {
    return { ...point, vx: Math.abs(vx), x: 0 + r };
  }
  if (x + r > w) {
    return { ...point, vx: -Math.abs(vx), x: w - r };
  }

  if (y - r < 0) {
    return { ...point, vy: Math.abs(vy), y: 0 + r };
  }
  if (y + r > h) {
    return { ...point, vy: -Math.abs(vy), y: h - r };
  }
  return point;
};

export const testCollision = (point, points) => {};

export const createLineNormal = () => {};

export const getDistance = (x0, x1, y0, y1) => {
  const distance = Math.sqrt(Math.pow(x1 - x0, 2) + Math.pow(y1 - y0, 2));
  return distance;
};

export const getAlpha = (x0, x1, distance) => {
  const cosA = (x1 - x0) / distance;
  return cosA;
};

export const newSpeedPoints = (points, point) => {
  const { x: x0, y: y0, id: id0, r: r0, vx: vx0, vy: vy0 } = point;

  let vxs = vx0;
  let vys = vy0;
  points.forEach((pt) => {
    if (pt.id !== id0) {
      const { x: x1, y: y1, r: r1 } = pt;
      const distance = getDistance(x0, x1, y0, y1);
      if (distance <= r0 + r1) {
        const cosA = getAlpha(x0, x1, distance);
        const gipo = Math.sqrt(Math.pow(vy0, 2) + Math.pow(vx0, 2));
        const angleA = (Math.acos(cosA) * 180) / Math.PI;
        const angleB = (Math.atan(vy0 / vx0) * 180) / Math.PI;
        const angleAB = angleA + angleB;

        vxs = gipo * Math.sin((Math.PI / 180) * angleAB);
        vys = gipo * Math.cos((Math.PI / 180) * angleAB);
        const D = (vxs - 0) * (vy0 - 0) - (vys - 0) * (vx0 - 0);

        vxs = D < 0 && x1 - x0 < 0 ? vxs : -1 * Math.abs(vxs);
        vys = D < 0 && y1 - y0 < 0 ? vys : -1 * Math.abs(vys);
      }
    }
  });
  return {
    vxs,
    vys,
    resPoint: point,
  };
};

/* export const newSpeedPoints = (points, point) => {
  const { x: x0, y: y0, id: id0, r: r0, vx, vy } = point;

  const { vx: vx1, vy: vy1 } = points.reduce(
    (acc, pt) => {
      const { x: x1, y: y1, id, vx, vy, r: r1 } = pt;
      if (id0 !== id) {
        const distance = getDistance(x0, x1, y0, y1);
        const cosA = getAlpha(x0, x1, distance);
        if (distance < r0 + r1) {
          const gipo = Math.sqrt(Math.pow(vy, 2) + Math.pow(vx, 2));
          const angleA = (Math.acos(cosA) * 180) / Math.PI;
          const angleB = (Math.atan(vy / vx) * 180) / Math.PI;
          const angleAB = angleA + angleB;
          console.log(gipo, vy, vx);

          const nVy = -1 * gipo * Math.sin((Math.PI / 180) * angleAB);
          const nVx = gipo * Math.cos((Math.PI / 180) * angleAB);
          console.log(nVx, nVy);
          return {
            vx1: nVx,
            vy1: nVy,
          };
        }
        return acc;
      }
      return acc;
    },
    { vx, vy }
  );
  return {
    ...point,
    vx: vx1,
    vy: vy1,
  };
}; */

/* 
const line = {
  x1: 0,
  y1: setCanWH().hC / 2,
  x2: setCanWH().wC,
  y2: setCanWH().hC / 1.2,
};

const cyrcleGen = () => {
  const { wC, hC } = setCanWH();
  const vx = Math.random() * 2 - 1;
  const vy = (1 - Math.abs(vx)) * (Math.round(Math.random()) * 2 - 1) || 1;
  return [...new Array(countP)].map(() => {
    return {
      id: Math.round(Math.random() * 1000),
      x: Math.round(Math.random() * (wC - 2 * rPoint) + rPoint),
      y: Math.round(Math.random() * (hC - 2 * rPoint) + rPoint),
      v: Math.round(Math.random() * speed) + 10,
      vx,
      vy,
      r: rPoint,
      color: `rgb(${Math.random() * 150 + 50}, ${Math.random() * 150 + 50}, ${
        Math.random() * 150 + 50
      })`,
    };
  });
}; */

// const D = (х3 - х1) * (у2 - у1) - (у3 - у1) * (х2 - х1)
// - Если D = 0 - значит, точка С лежит на прямой АБ.
// - Если D < 0 - значит, точка С лежит слева от прямой.
// - Если D > 0 - значит, точка С лежит справа от прямой.
/* 
const fnCross = (points) => {

  return points.map((pt, i) => {
    if (id !== pt.id) {
      const cosAlpha = (x - pt.x) / (y - pt.y);
      const katTX = r * cosAlpha;
      const katTY = katTX / cosAlpha;

      const x1 = x + katTX;
      const y1 = y + katTY;

      const kat1 = r / cosAlpha;
      const x2 = x + Math.sqrt(Math.pow(kat1, 2) + Math.pow(r, 2));
      const y2 = y;
      //       if (i === 1) {
      //   console.log(x1, y1);
      // }
      return {
        id: Math.round(Math.random() * 1000),
        x: x1,
        y: y1,
        r: 8,
        color: "#fff",
      };
    }
  });
}; */
/* 
const test = (point, points) => {
  const { wC: w, hC: h } = setCanWH();
  const { x, y, vx, vy, r, id } = point;
  let sx = vx;
  let sy = vy;

  const { x1, y1, x2, y2 } = line;

  const D = (x - x1) * (y2 - y1) - (y - y1) * (x2 - x1) < 0;

  const distToLine = Math.round(
    Math.abs(
      Math.abs((y2 - y1) * x - (x2 - x1) * y + x2 * y1 - y2 * x1) /
        Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2))
    )
  );

  if (D && distToLine < r) {
    sy = Math.abs(vy);
  }

  if (x - r <= 0) {
    sx = -vx;
  }
  if (x + r >= w) {
    sx = -vx;
  }

  if (y - r <= 0) {
    sy = -vy;
  }
  if (y + r >= h) {
    sy = -vy;
  }

  return { vx: sx, vy: sy };
};
 */
/* const move = (points = []) => {
  const newPoints = fnCross();
  return points
    .map((point) => {
      const { vx, vy } = test(point, points);
      return {
        ...points,
        //x: point.x + vx * point.v,
        // y: point.y + vy * point.v,
        //vx,
        //vy,
      };
    })
    .concat(newPoints);
};

const drawLine = (ctx, line) => {
  const { x1, y1, x2, y2 } = line;
  ctx.strokeStyle = "rgb(255, 150, 150)";
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();
};

const drawBacground = (ctx, wC, hC) => {
  ctx.fillStyle = "rgb(50, 50, 50)";
  ctx.fillRect(0, 0, wC, hC);
};

const drawCyrcle = (ctx, points) => {
  points.forEach(({ x, y, r, color }) => {
    ctx.beginPath();
    ctx.fillStyle = color;
    //ctx.strokeStyle = "rgb(255, 150, 150)";
    ctx.arc(x, y, r, 0, Math.PI * 2, true);
    ctx.fill();
    //ctx.stroke();
  });
};

const draw = (wC, hC, points) => {
  const ctx = canvas.getContext("2d");
  drawBacground(ctx, wC, hC);
  drawCyrcle(ctx, points);
  drawLine(ctx, line);
};

const anim = (pts) => {
  let points = pts;
  requestAnimationFrame(function () {
    const { wC, hC } = setCanWH();
    points = move(points);
    draw(wC, hC, points);
    requestAnimationFrame(() => anim(points));
  });
};

const start = (pts) => {
  anim(pts);
}; */

/* const cosAlpha = x - pt.x / y - pt.y;
    const katTX = r * cosAlpha;
    const katTY = katTX / cosAlpha;

    const x1 = x + katTX;
    const y1 = y + katTY;

    const kat1 = r / cosAlpha;
    const x2 = x + Math.sqrt(Math.pow(kat1, 2) + Math.pow(r, 2));
    const y2 = y;

    const D = (x - x1) * (y2 - y1) - (y - y1) * (x2 - y1); */

/*   points.forEach((pt) => {
    const katX = Math.abs(x - pt.x);
    const katY = Math.abs(y - pt.y);
    const coof = katY / katX;
    const distance = Math.sqrt(Math.pow(katX, 2) + Math.pow(katY, 2));
    const distCollis = r + pt.r;
    if (distance < distCollis && id !== pt.id) {
      if (coof > 0) {
        sx = Math.abs(vx) * coof;
        sy = Math.abs(vy) * (1 / coof);
      }
      sx = -vx;
      sy = -vy;
    }
  }); */
