export const initCanvas = () => {
  const canvas = document.getElementById("canvas");
  const ctx = canvas.getContext("2d");
  const btnSTART = document.getElementById("start");
  const btnSTOP = document.getElementById("stop");
  const fpxDisplay = document.getElementById("fpsDisplay");

  canvas.setAttribute("width", document.body.clientWidth);
  canvas.setAttribute("height", document.body.clientHeight);
  ctx.clearRect(0, 0, document.body.clientWidth, document.body.clientHeight);
  return { canvas, ctx, btnSTART, btnSTOP, fpxDisplay };
};

// const setCanWH = (canvas) => {
//   const { clientWidth: wC, clientHeight: hC } = canvas;
//   return { wC, hC };
// };
