export const drawCircle = (
  ctx,
  x = 0,
  y = 0,
  r = 10,
  color = "black",
  fill = false,
  width = 1
) => {
  ctx.beginPath();
  ctx.lineWidth = width;
  ctx.strokeStyle = color;
  ctx.fillStyle = color;
  ctx.arc(x, y, r, 0, Math.PI * 2, true);
  fill ? ctx.fill() : ctx.stroke();
};
