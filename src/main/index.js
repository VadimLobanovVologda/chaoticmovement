import { drawCircle } from "./draw";

export const main = (canvas, ctx, balls) => {
  balls.forEach((ball) => {
    const { x, y, r, c } = ball;
    drawCircle(ctx, x, y, r, c, true);
  });
  return balls;
};
