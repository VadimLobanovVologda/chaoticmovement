const testCollision = (balls, x0, y0, r0) => {
  const collision = balls.reduce((test, ball) => {
    if (balls.length && !test) {
      const { x: x1, y: y1, r: r1 } = ball;
      const distance = Math.sqrt(Math.pow(x1 - x0, 2) + Math.pow(y1 - y0, 2));
      return distance < r1 + r0;
    }
    return test;
  }, false);
  return collision;
};

const getPropsBall = (acc, radius, cw, ch, iter = 0) => {
  const colorR = Math.round(Math.random() * 255);
  const colorG = Math.round(Math.random() * 255);
  const colorB = Math.round(Math.random() * 255);
  const r = Math.round(Math.random() * (radius - 10) + 10);
  const x = Math.round(Math.random() * (cw - 2 * r) + r);
  const y = Math.round(Math.random() * (ch - 2 * r) + r);
  const c = `rgb(${colorR},${colorG},${colorB})`;
  const id = Math.round(Math.random() * 1024);

  if (testCollision(acc, x, y, r, id)) {
    return getPropsBall(acc, radius, cw, ch, iter + 1);
  }

  return { x, y, r, c, id };
};

export const ballsGenerate = (count, radius, canvas) => {
  const { clientWidth: cw, clientHeight: ch } = canvas;

  const balls = [...new Array(count)].reduce((acc) => {
    const { x, y, r, c, id } = getPropsBall(acc, radius, cw, ch);
    return [...acc, { x, y, r, c, id }];
  }, []);
  return balls;
};
