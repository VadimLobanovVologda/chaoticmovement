export const getFps = (now, date) => {
  const fps = Math.round(1000 / (now - date));
  return fps === Infinity ? "~" : fps;
};

export const setBorderCanvas = (canvas) => {
  canvas.setAttribute("width", document.body.clientWidth);
  canvas.setAttribute("height", document.body.clientHeight);
};
